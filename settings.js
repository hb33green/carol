const Discord = require("discord.js");
const help = require("./help.js");
const fs = require("fs");
const defSettings = require("./settings.json");

module.exports = {
    settings: (msg, pre, args) => {
        if (args.length > 0 && args.length < 2) {
            help.helpS(msg, pre);
            return;
        }

        let rawData = fs.readFileSync("./bot_settings.json");
        let botSettings = JSON.parse(rawData);

        if (args.length > 0 && msg.member.hasPermission("ADMINISTRATOR")) {
            var change = args[args.length - 1];
            var search = args[0];
            for (i = 1; i < args.length - 1; i++) {
                search += " " + args[i]
            }

            change = change.toLowerCase();
            search = search.toLowerCase();
            try {
                switch (search) {
                    case "keep before":
                        if (!(change === "true" || change === "false")) {
                            throw null;
                        }
                        change = (change === "true");
                        botSettings["keep before"] = change;
                        break;
                    case "count":
                        change = parseInt(change, 10);
                        if (isNaN(change)) {
                            throw null;
                        }
                        botSettings.count = change;
                        break;
                    case "response time":
                        change = parseInt(change, 10);
                        if (isNaN(change)) {
                            throw null;
                        }
                        botSettings["response time"] = change;
                        break;
                }
            } catch {
                help.helpS(msg, pre);
                return;
            }
        } else if (args.length > 0) {
            help.helpSNoAccess(msg, pre);
            return;
        }

        let backToRaw = JSON.stringify(botSettings, null, 2);
        fs.writeFileSync("./bot_settings.json", backToRaw);

        var post = new Date();
        post = post.getTime() / 1000;

        const embed = new Discord.RichEmbed()
            .setColor("WHITE")
            .setTitle("Settings")
            .addField("Prefix", defSettings.prefix.toUpperCase(), true)
            .addField("Keep Before", botSettings["keep before"], true)
            .addField("Count", `${botSettings.count} iterations`, true)
            .addField("Response Timeout", `${botSettings["response time"]} milliseconds`, true)
            .setURL(defSettings["git url"])
            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
            .setTimestamp();

        msg.channel.send(embed);
    }
}