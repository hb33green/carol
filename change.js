const Discord = require("discord.js");
const package = require("./package.json");

module.exports = {
    change: (msg, pre) => {

        var post = new Date();
        post = post.getTime() / 1000;

        const embed = new Discord.RichEmbed()
            .setColor("DARKER_GREY")
            .setTitle("Changelog")
            .addField("Version", package.version)
            .attachFile("./changelog.txt")
            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
            .setTimestamp();

        msg.channel.send(embed);
    }
}