const package = require("./package.json");
const Discord = require("discord.js");

module.exports = {
    help: (msg, pre) => {

        var post = new Date();
        post = post.getTime() / 1000;

        const embed = new Discord.RichEmbed()
            .setColor("DARK_RED")
            .setAuthor("Help")
            .setDescription(package.description)
            .addField("Author:", `CAROL is developed by: ${package.author}`, true)
            .addField("Version:", `${package.version}`, true)
            .addField("Usage:", "CAROL <command>\n\nWhere <command> is one of:\n-c, -h, -i, -l, -o, -r, -s\n")
            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
            .setTimestamp();

        msg.channel.send(embed);
    },

    helpI: (msg, pre) => {
        var post = new Date();
        post = post.getTime() / 1000;

        const embed = new Discord.RichEmbed()
            .setColor("DARK_VIVID_PINK")
            .setAuthor("Info")
            .setDescription("Hmm... It seems as if you tried to use the -i function without providing a valid search parameter. Try looking at the usage example below or check your spelling! This function can be used to look up certain rules like attributes, feats, or banes. It also utilizes Fuzzy Search to try and give you the best possible result despite any inaccuracies that may be present in your queries.")
            .addField("Usage:", "CAROL -i <item>\n\nWhere <item> is on of the following:\nAttribute, Bane, Boon, Defense, Feat, Flaw, or Perk.")
            .addField("Example", "CAROL -i Climbing")
            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
            .setTimestamp();

        msg.channel.send(embed);
    },

    helpL: (msg, pre) => {
        var post = new Date();
        post = post.getTime() / 1000;

        const embed = new Discord.RichEmbed()
            .setColor("DARK_GOLD")
            .setAuthor("Luck")
            .setDescription("Hmm... It seems as if you tried to use the -l function without providing a valid search parameter. Try looking at the usage example below.")
            .addField("Usage:", "CAROL -l [<user> <session> <clear>]\n\nWhere <user> is an optional parameter that denotes the user that you're applying the operation to. This parameter needs to be in the form of a mention. This parameter defaults to you if left blank.\nWhere <session> is an optional parameter denoted simply with an S that will define whether the current action should be applied to the ongoing session.\nWhere <clear> is an optional parameter denoted by a C that will clear the data of the member listed. People can only clear their own data except for Administrators who can clear anyones data.")
            .addField("Example", "CAROL -l S CA")
            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
            .setTimestamp();

        msg.channel.send(embed);
    },

    helpLDataNull: (msg, pre) => {
        var post = new Date();
        post = post.getTime() / 1000;

        const embed = new Discord.RichEmbed()
            .setColor("DARK_GOLD")
            .setAuthor("Luck")
            .setDescription("Hmm... It seems as if you tried to use the -l function on a user that doesn't have any data yet.")
            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
            .setTimestamp();

        msg.channel.send(embed);
    },

    helpLNoAccess: (msg, pre) => {
        var post = new Date();
        post = post.getTime() / 1000;

        const embed = new Discord.RichEmbed()
            .setColor("DARK_GOLD")
            .setAuthor("Luck")
            .setDescription("Hmm... It seems as if you tried to clear data on a user that you don't have permission to edit. If you aren't an Administrator you can't clear data that isn't your own.")
            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
            .setTimestamp();

        msg.channel.send(embed);
    },

    helpLClear: (msg, pre) => {
        var post = new Date();
        post = post.getTime() / 1000;

        const embed = new Discord.RichEmbed()
            .setColor("DARK_GOLD")
            .setAuthor("Luck")
            .setDescription("Data successfully cleared.")
            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
            .setTimestamp();

        msg.channel.send(embed);
    },

    helpO: (msg, pre) => {
        var post = new Date();
        post = post.getTime() / 1000;

        const embed = new Discord.RichEmbed()
            .setColor("DARK_GREEN")
            .setAuthor("Open Legend Roller")
            .setDescription("Hmm... It seems as if you tried to use the -o function without providing a valid parameter. Try looking at the usage example below.")
            .addField("Usage:", "CAROL -o <level> [<advantage> <challenge> <legend> <vicious>]\n\nWhere <level> is the level of the attribute check (between 0 and 10 inclusive).\nWhere <advantage> an optional parameter that denotes the amount of advantage or disadvantage (written with a + or - respectively followed by the amount of advantage and disadvantage).\nWhere <challenge> is an optional parameter that represents the challenge rating (written with a C followed by the challenge rating).\nWhere <legend> is an optional parameter that denotes the number of legend points used (written with an L followed by the amount of legend points that you wish to use).\nWhere <vicious> is an optional parameter for the Vicious Strike feat (denoted by including or omitting a V).\nWhere <destructive> is an optional parameter for the Destructive Trance feat (denoted by including or omitting a T).")
            .addField("Example", "CAROL -o 3 +1 C16 L1 V T")
            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
            .setTimestamp();

        msg.channel.send(embed);
    },

    helpR: (msg, pre) => {
        var post = new Date();
        post = post.getTime() / 1000;

        const embed = new Discord.RichEmbed()
            .setColor("DARK_BLUE")
            .setAuthor("Regular Roller")
            .setDescription("Hmm... It seems as if you tried to use the -r function without providing a valid parameter. Try looking at the usage example below.")
            .addField("Usage:", "CAROL -r <dieCount>D<sides>[K/L<keep>]... [<modifier> C<challenge> <vicious> <destructive>]\n\nWhere <dieCount> is the number of the particular die that follows the D.\nWhere <side> is how many sides this particular die has.\nWhere <keep> is an optional parameter that represents the amount of this particular die that will be kept. This parameter needs to be headed with a K to denote keeping the highest and an L to denote keeping the lowest. The value placed in keep needs to be between 0 and <diecount> inclusive.\nWhere <modifier> is an optional parameter that adds an overall modifier to the whole die roll.\nWhere <challenge> is an optional parameter that represents the challenge rating (written with a C followed by the challenge rating).\nWhere <vicious> is an optional parameter for the Vicious Strike feat (denoted by including or omitting a V).\nWhere <destructive> is an optional parameter for the Destructive Trance feat (denoted by including or omitting a T).\nPlease note that none of these values can be negative.")
            .addField("Note", "All dice will automatically explode on their highest value.")
            .addField("Example", "CAROL -r 1D4 3D8K1 10 C20 V T\n")
            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
            .setTimestamp();

        msg.channel.send(embed);
    },

    helpS: (msg, pre) => {
        var post = new Date();
        post = post.getTime() / 1000;

        const embed = new Discord.RichEmbed()
            .setColor("WHITE")
            .setAuthor("Settings")
            .setDescription("Hmm... It seems as if you tried to use the -s function without providing a valid parameter. Try looking at the usage example below.")
            .addField("Usage:", "CAROL -s [<setting> <new>]\n\nWhere <setting> is an optional parameter that must be paired with <new> that specifies the setting that you wish to change. This can be any of the following values:\nKeep Before, Count, or Response Time.\nWhere <new> is an optional parameter that must be paired with <setting> that specifies the new value for the particular setting.")
            .addField("Example", "CAROL -s count 10000\n")
            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
            .setTimestamp();

        msg.channel.send(embed);
    },

    helpSNoAccess: (msg, pre) => {
        var post = new Date();
        post = post.getTime() / 1000;

        const embed = new Discord.RichEmbed()
            .setColor("WHITE")
            .setAuthor("Settings")
            .setDescription("Hmm... It seems as if you tried to change the settings without having the proper permissions. Contact an Administrator to ask about changing the settings.")
            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
            .setTimestamp();

        msg.channel.send(embed);
    },
}