module.exports = {
    roller: (sidesOfDice, numOfDice, keepsOfDice, keepBefore, vicious, destructive, modifier) => {
        const arrSum = arr => arr.reduce((a,b) => a + b, 0);

        var finalSum = 0;
        var rolls = [];
        for (j = 0; j < sidesOfDice.length; j++) {
            var outcomes = [];
            var hasExploded = 0;
            for (k = 0; k < numOfDice[j]; k++) {
                var firstRoll = []
                firstRoll = [Math.floor((Math.random() * sidesOfDice[j]) + 1)]
                if (keepBefore && hasExploded >= Math.abs(keepsOfDice[j])) {
                    outcomes = [...outcomes, ...[firstRoll]];
                    continue;
                }
                if (arrSum(firstRoll) % sidesOfDice[j] == 0 || (arrSum(firstRoll) % sidesOfDice[j] ==  sidesOfDice[j] - 1 && destructive)) {
                    hasExploded++;
                }

                while (arrSum(firstRoll) % sidesOfDice[j] == 0 || (arrSum(firstRoll) % sidesOfDice[j] ==  sidesOfDice[j] - 1 && destructive)) {
                    if (firstRoll[firstRoll.length - 1] == 1) {
                        break;
                    }
                    if (vicious && sidesOfDice[j] == 20) {
                        firstRoll = [...firstRoll, Math.max(Math.floor((Math.random() * sidesOfDice[j]) + 1), Math.floor((Math.random() * sidesOfDice[j]) + 1))];
                    } else {
                        firstRoll = [...firstRoll, Math.floor((Math.random() * sidesOfDice[j]) + 1)];
                    }
                }
                outcomes = [...outcomes, ...[firstRoll]];
            }
            if (keepsOfDice[j] < 0) {
                outcomes.sort((a, b) => arrSum(a) - arrSum(b));
            } else if (keepsOfDice[j] > 0) {
                outcomes.sort((a, b) => arrSum(b) - arrSum(a));
            }
            var individualSum = 0;
            for (k = 0; k < Math.abs(keepsOfDice[j]); k++) {
                individualSum += arrSum(outcomes[k]);
            }
            finalSum = finalSum + individualSum;
            rolls = [...rolls, ...[outcomes]]
        }
        finalSum = finalSum + modifier;
        returnArr = [...rolls, finalSum];
        return returnArr;
    }
}