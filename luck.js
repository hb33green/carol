const Discord = require("discord.js");
const help = require("./help.js");
const fs = require("fs");

module.exports = {
    luck: (msg, pre, args) => {
        if (args.length < 0 || args.length > 3 || msg.mentions.users.size > 1) {
            help.helpL(msg, pre);
            return;
        }

        let rawData = fs.readFileSync("./bot_settings.json");
        let unRawData = JSON.parse(rawData);
        unRawData["keep before"] = true;
        let rawedData = JSON.stringify(unRawData, null, 2);
        fs.writeFileSync("./bot_settings.json", rawedData);

        var session = false;
        var clear = false;
        var useAuthor = false;
        var usersData;
        var isAdmin = false;
        try {
            if (msg.member.hasPermission("ADMINISTRATOR")) {
                isAdmin = true;
            }

            for (i = 0; i < args.length; i++) {
                args[i] = args[i].toLowerCase();
                if (args[i].includes("<@")) {
                    continue;
                } else if (args[i] == "c") {
                    clear = true;
                } else if (args[i] == "s") {
                    session = true;
                } else {
                    throw null;
                }
            }
            if (msg.mentions.users.size == 0) {
                useAuthor = true;
            } else if (msg.mentions.users.first().bot) {
                throw null;
            } else if (msg.author.id == msg.mentions.users.first().id) {
                useAuthor = true;
            }

        } catch {
            help.helpL(msg, pre);
            return;
        }

        if (!isAdmin && clear && !useAuthor) {
            help.helpLNoAccess(msg, pre);
            return;
        }

        let rawSession = fs.readFileSync("./session_luck.json");
        let sessionLucky = JSON.parse(rawSession);
        let raw = fs.readFileSync("./luck.json");
        let lucky = JSON.parse(raw);

        try {
            if (useAuthor) {
                if (session) {
                    if (clear) {
                        for (i = 0; i < sessionLucky.users.length; i++) {
                            if (sessionLucky.users[i].hasOwnProperty(msg.author.id)) {
                                sessionLucky.users.splice(i, 1);
                                let backToRawSession = JSON.stringify(sessionLucky, null, 2);
                                fs.writeFileSync("./session_luck.json", backToRawSession);
                                help.helpLClear(msg, pre);
                                return;
                            }
                        }
                    } else {
                        for (i = 0; i < sessionLucky.users.length; i++) {
                            if (sessionLucky.users[i].hasOwnProperty(msg.author.id)) {
                                usersData = sessionLucky.users[i][msg.author.id];
                                break;
                            }
                        }
                    }
                } else {
                    if (clear) {
                        for (i = 0; i < lucky.users.length; i++) {
                            if (lucky.users[i].hasOwnProperty(msg.author.id)) {
                                lucky.users.splice(i, 1);
                                let backToRaw = JSON.stringify(lucky, null, 2);
                                fs.writeFileSync("./luck.json", backToRaw);
                                help.helpLClear(msg, pre);
                                return;
                            }
                        }
                    } else {
                        for (i = 0; i < lucky.users.length; i++) {
                            if (lucky.users[i].hasOwnProperty(msg.author.id)) {
                                usersData = lucky.users[i][msg.author.id];
                                break;
                            }
                        }
                    }
                }
            } else {
                if (session) {
                    if (clear) {
                        for (i = 0; i < sessionLucky.users.length; i++) {
                            if (sessionLucky.users[i].hasOwnProperty(msg.mentions.users.first().id)) {
                                sessionLucky.users.splice(i, 1);
                                let backToRawSession = JSON.stringify(sessionLucky, null, 2);
                                fs.writeFileSync("./session_luck.json", backToRawSession);
                                help.helpLClear(msg, pre);
                                return;
                            }
                        }
                    } else {
                        for (i = 0; i < sessionLucky.users.length; i++) {
                            if (sessionLucky.users[i].hasOwnProperty(msg.mentions.users.first().id)) {
                                usersData = sessionLucky.users[i][msg.mentions.users.first().id];
                                break;
                            }
                        }
                    }
                } else {
                    if (clear) {
                        for (i = 0; i < lucky.users.length; i++) {
                            if (lucky.users[i].hasOwnProperty(msg.mentions.users.first().id)) {
                                lucky.users.splice(i, 1);
                                let backToRaw = JSON.stringify(lucky, null, 2);
                                fs.writeFileSync("./luck.json", backToRaw);
                                help.helpLClear(msg, pre);
                                return;
                            }
                        }
                    } else {
                        for (i = 0; i < lucky.users.length; i++) {
                            if (lucky.users[i].hasOwnProperty(msg.mentions.users.first().id)) {
                                usersData = lucky.users[i][msg.mentions.users.first().id];
                                break;
                            }
                        }
                    }
                }
            }
            if (typeof usersData == 'undefined') {
                throw null;
            }
        } catch {
            help.helpLDataNull(msg, pre);
            return;
        }

        var avgSuccessPercent;
        if (usersData["success count"] == 0) {
            avgSuccessPercent = 0;
        } else {
            avgSuccessPercent = Math.round(usersData["success percent"] / usersData["success count"]);
        }

        var avgFailurePercent;
        if (usersData["failure count"] == 0) {
            avgFailurePercent = 0;
        } else {
            avgFailurePercent = Math.round(usersData["failure percent"] / usersData["failure count"]);
        }

        const embed = new Discord.RichEmbed()
            .setColor("DARK_GOLD")
            .setAuthor("Luck")
            .addField("Success", `Average: ${avgSuccessPercent}%\n${usersData["success count"]} Successful Rolls`, true)
            .addField("Failure", `Average: ${avgFailurePercent}%\n${usersData["failure count"]} Failed Rolls`, true)
            .addField("Total Rolls", `${usersData["success count"] + usersData["failure count"]} rolls`, true);

        if (session) {
            embed
                .setTitle("Session Luck");
        } else {
            embed
                .setTitle("Lifetime Luck");
        }

        var post = new Date();
        post = post.getTime() / 1000;

        embed
            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
            .setTimestamp();

        msg.channel.send(embed);
    }
}