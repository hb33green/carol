const Discord = require("discord.js");
const info = require("./info.json");
const fuzzy = require("fuzzyset.js");
const help = require("./help.js");

module.exports = {
    info: (msg, pre, args) => {
        if (args.length <= 0) {
            help.helpI(msg, pre);
            return;
        }

        var search = args[0];
        for (i = 1; i < args.length; i++) {
            search += " " + args[i];
        }

        var fs = FuzzySet([]);
        for (i = 0; i < info.info.length; i++) {
            for (j = 0; j < info.info[i].content.length; j++) {
                fs.add(info.info[i].content[j].name)
            }
        }
        var results = fs.get(search);

        if (results == null) {
            help.helpI(msg, pre);
            return;
        }

        var foundObj;
        var objType;
        for (i = 0; i < info.info.length; i++) {
            for (j = 0; j < info.info[i].content.length; j++) {
                if (results[0][1] == info.info[i].content[j].name) {
                    foundObj = info.info[i].content[j];
                    break;
                }
            }
            if (foundObj != null) {
                objType = info.info[i].type;
                break;
            }
        }

        const embed = new Discord.RichEmbed()
            .setColor("DARK_VIVID_PINK")
            .setTitle(foundObj.name)
            .setURL(foundObj.url)
            .setDescription(foundObj.description)
            .addField("Percent Match", `${Math.round(results[0][0] * 100)}% Match`, true);

        switch (objType) {
            case "attribute":
                embed
                    .setAuthor("Attribute")
                    .addField("Type", foundObj.type, true);
                break;
            case "defense":
                embed
                    .setAuthor("Defense")
                    .addField("Calculation", foundObj.calculation, true);
                break;
            case "feat":
                embed
                    .setAuthor("Feat")
                    .addField("Cost", foundObj.cost, true)
                    .addField("Prerequisites", foundObj.prerequisites, true);

                if (foundObj.effect.length > 1024) {
                    embed.addField("Effect", foundObj.effect.substring(0, 1020) + "...");
                } else {
                    embed.addField("Effect", foundObj.effect);
                }

                if (foundObj.special != null) {
                    if (foundObj.special.length > 1024) {
                        embed.addField("Special", foundObj.special.substring(0, 1020) + "...");
                    } else {
                        embed.addField("Special", foundObj.special);
                    }
                }
                break;
            case "bane":
                embed
                    .setAuthor("Bane")
                    .addField("Duration", foundObj.duration, true)
                    .addField("Power Level", foundObj["power level"], true)
                    .addField("Attack Attributes", foundObj["attack attributes"], true)
                    .addField("Attack", foundObj.attack, true);

                if (foundObj.effect.length > 1024) {
                    embed.addField("Effect", foundObj.effect.substring(0, 1020) + "...");
                } else {
                    embed.addField("Effect", foundObj.effect);
                }

                if (foundObj.special != null) {
                    if (foundObj.special.length > 1024) {
                        embed.addField("Special", foundObj.special.substring(0, 1020) + "...");
                    } else {
                        embed.addField("Special", foundObj.special);
                    }
                }
                break;
            case "boon":
                embed
                    .setAuthor("Boon")
                    .addField("Duration", foundObj.duration, true)
                    .addField("Invocation Time", foundObj["invocation time"], true)
                    .addField("Power Level", foundObj["power level"], true)
                    .addField("Attributes", foundObj.attributes, true);

                if (foundObj.effect.length > 1024) {
                    embed.addField("Effect", foundObj.effect.substring(0, 1020) + "...");
                } else {
                    embed.addField("Effect", foundObj.effect);
                }

                if (foundObj.special != null) {
                    if (foundObj.special.length > 1024) {
                        embed.addField("Special", foundObj.special.substring(0, 1020) + "...");
                    } else {
                        embed.addField("Special", foundObj.special);
                    }
                }
                break;
            case "perk":
                embed
                    .setAuthor("Perk");
                break;
            case "flaw":
                embed
                    .setAuthor("Flaw");
                break;
            default:
                help.helpI(msg, pre);
                return;
        }

        var post = new Date();
        post = post.getTime() / 1000;

        embed
            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
            .setTimestamp();

        msg.channel.send(embed);
    }
}