const Discord = require("discord.js");
const help = require("./help.js");
const botSettings = require("./bot_settings.json");
const roller = require("./roller.js");
const prob = require("./probability_calculator.js");
const fs = require("fs");

module.exports = {
    roll: (msg, pre, args) => {
        if (args.length <= 0) {
            help.helpR(msg, pre);
            return;
        }

        let rawSession = fs.readFileSync("./session_luck.json");
        let sessionLucky = JSON.parse(rawSession);
        let raw = fs.readFileSync("./luck.json");
        let lucky = JSON.parse(raw);

        var responseTime = botSettings["response time"];

        var roll = botSettings.yes;
        var cancel = botSettings.no;

        var sidesOfDice = [];
        var numOfDice = [];
        var keepsOfDice = [];
        // var keepBefore = botSettings["keep before"];
        var vicious = false;
        var destructive = false;
        var modifier = 0;
        var cr;
        // var count = botSettings.count;

        let rawS = fs.readFileSync("./bot_settings.json");
        let sett = JSON.parse(rawS);
        var keepBefore = sett["keep before"];
        var count = sett.count;

        try {
            for (i = 0; i < args.length; i++) {
                args[i] = args[i].toLowerCase();
                if (args[i].includes("d")) {
                    var temp = args[i].split("d");
                    if (temp[0] == "") {
                        numOfDice.push(1);
                    } else {
                        numOfDice.push(parseInt(temp[0], 10));
                    }
                    temp = temp[1];

                    if (args[i].includes("k") && args[i].includes("l")) {
                        throw null;
                    }

                    if (args[i].includes("k")) {
                        temp = temp.split("k");
                        keepsOfDice.push(parseInt(temp[1], 10));
                    } else if (args[i].includes("l")) {
                        temp = temp.split("l");
                        keepsOfDice.push(-1 * parseInt(temp[1], 10))
                    } else {
                        keepsOfDice.push(numOfDice[numOfDice.length - 1]);
                    }

                    if (Math.abs(keepsOfDice[keepsOfDice.length - 1]) > numOfDice[numOfDice.length - 1]) {
                        throw null;
                    }

                    sidesOfDice.push(parseInt(temp, 10));
                } else if (args[i].includes("c")) {
                    if (cr != null) {
                        throw null;
                    }
                    var temp = args[i].split("c");

                    cr = parseInt(temp[1], 10);

                    if (cr < 0 || isNaN(cr)) {
                        throw null;
                    }
                } else if (args[i].includes("v")) {
                    if (vicious || args[i].length > 1) {
                        throw null;
                    }
                    vicious = true;
                } else if (args[i].includes("t")) {
                    if (destructive || args[i].length > 1) {
                        throw null;
                    }
                    destructive = true;
                } else {
                    modifier += parseInt(args[i], 10);

                    if (isNaN(modifier)) {
                        throw null;
                    }
                }
            }
            for (j = 0; j < numOfDice.length; j++) {
                if (isNaN(numOfDice[j]) || isNaN(sidesOfDice[j]) || isNaN(keepsOfDice[j]) || sidesOfDice[j] == 1) {
                    throw null;
                }
            }
        } catch {
            help.helpR(msg, pre);
            return;
        }

        if (cr != null) {
            results = prob.probabilityCalculator(sidesOfDice, numOfDice, keepsOfDice, keepBefore, vicious, destructive, modifier, cr, count);
            var post = new Date();
            post = post.getTime() / 1000;

            const embed = new Discord.RichEmbed()
                .setColor("DARK_BLUE")
                .setAuthor("Regular Roller")
                .setTitle(`${results[1]}% Chance of Success.`)
                .setDescription(`You have ${Math.round(responseTime / 1000)} seconds to respond before this roll auto-cancels.`)
                .addField("Success Rate", `${results[1]}%`, true)
                .addField("Failure Rate", `${results[0]}%`, true)
                .addField("Challenge Rating", `${cr}`, true)
                .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
                .setTimestamp();

            embed.addField("Other Parameters", `Vicious: ${vicious}\nDestructive: ${destructive}\nKeep Before: ${keepBefore}\nCount: ${count}`, true)

            msg.channel.send(embed).then(sent => {
                sent.react(roll).then(() => {
                    sent.react(cancel);
                });
                sent.awaitReactions((reaction, user) => { return (reaction.emoji.name === roll || reaction.emoji.name === cancel) && user.id === msg.author.id; },
                    { max: 1, time: responseTime, errors: ["time"] })
                    .then(collected => {
                        if (collected.firstKey() == roll) {
                            var hasData = false;
                            var hasSessionData = false;
                            for (i = 0; i < sessionLucky.users.length; i++) {
                                if (sessionLucky.users[i].hasOwnProperty(msg.author.id)) {
                                    hasSessionData = true;
                                }
                            }
                            for (i = 0; i < lucky.users.length; i++) {
                                if (lucky.users[i].hasOwnProperty(msg.author.id)) {
                                    hasData = true;
                                }
                            }

                            var result = roller.roller(sidesOfDice, numOfDice, keepsOfDice, keepBefore, vicious, destructive, modifier);

                            finalSum = result.pop();

                            const arrSum = arr => arr.reduce((a, b) => a + b, 0);
                            var temp = "";
                            for (i = 0; i < sidesOfDice.length; i++) {
                                temp += "d" + sidesOfDice[i].toString() + ":[";
                                for (j = 0; j < numOfDice[i]; j++) {
                                    temp += arrSum(result[i][j]);
                                    if (result[i][j].length > 1) {
                                        temp += " (";
                                        for (k = 0; k < result[i][j].length; k++) {
                                            temp += result[i][j][k] + " + ";
                                        }
                                        temp = temp.slice(0, temp.length - 3);
                                        temp += ")";
                                    }
                                    temp += ", "
                                }
                                temp = temp.slice(0, temp.length - 2);
                                temp += "] ";
                            }

                            if (!hasData) {
                                var newObj = {
                                    [msg.author.id]: {
                                        "success percent": 0,
                                        "success count": 0,
                                        "failure percent": 0,
                                        "failure count": 0
                                    }
                                }

                                lucky.users.push(newObj);
                            }

                            if (!hasSessionData) {
                                var newObj = {
                                    [msg.author.id]: {
                                        "success percent": 0,
                                        "success count": 0,
                                        "failure percent": 0,
                                        "failure count": 0
                                    }
                                }

                                sessionLucky.users.push(newObj);
                            }

                            var outcomeString = "";
                            if (finalSum >= cr) {
                                outcomeString = "Success!";
                                for (i = 0; i < sessionLucky.users.length; i++) {
                                    if (sessionLucky.users[i].hasOwnProperty(msg.author.id)) {
                                        sessionLucky.users[i][msg.author.id]["success percent"] += results[1];
                                        sessionLucky.users[i][msg.author.id]["success count"] += 1;
                                        break;
                                    }
                                }
                                for (i = 0; i < lucky.users.length; i++) {
                                    if (lucky.users[i].hasOwnProperty(msg.author.id)) {
                                        lucky.users[i][msg.author.id]["success percent"] += results[1];
                                        lucky.users[i][msg.author.id]["success count"] += 1;
                                        break;
                                    }
                                }
                            } else {
                                outcomeString = "Failure...";
                                for (i = 0; i < sessionLucky.users.length; i++) {
                                    if (sessionLucky.users[i].hasOwnProperty(msg.author.id)) {
                                        sessionLucky.users[i][msg.author.id]["failure percent"] += results[0];
                                        sessionLucky.users[i][msg.author.id]["failure count"] += 1;
                                        break;
                                    }
                                }
                                for (i = 0; i < lucky.users.length; i++) {
                                    if (lucky.users[i].hasOwnProperty(msg.author.id)) {
                                        lucky.users[i][msg.author.id]["failure percent"] += results[0];
                                        lucky.users[i][msg.author.id]["failure count"] += 1;
                                        break;
                                    }
                                }
                            }

                            let backToRawSession = JSON.stringify(sessionLucky, null, 2);
                            let backToRaw = JSON.stringify(lucky, null, 2);
                            fs.writeFileSync("./session_luck.json", backToRawSession);
                            fs.writeFileSync("./luck.json", backToRaw);

                            post = new Date();
                            post = post.getTime() / 1000;

                            const embedCont = new Discord.RichEmbed()
                                .setColor("DARK_BLUE")
                                .setAuthor("Regular Roller")
                                .setTitle(`${outcomeString} ${finalSum}`)
                                .setDescription(`Dice: ${temp}`)
                                .addField("Success Rate", `${results[1]}%`, true)
                                .addField("Failure Rate", `${results[0]}%`, true)
                                .addField("Challenge Rating", `${cr}`, true)
                                .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
                                .setTimestamp()
                                .addField("Other Parameters", `Vicious: ${vicious}\nDestructive: ${destructive}\nKeep Before: ${keepBefore}\nCount: ${count}`, true);

                            msg.channel.send(embedCont).then(() => {
                                sent.delete();
                            });
                        } else if (collected.firstKey() == cancel) {
                            sent.delete();
                            msg.react(cancel);
                        }
                    })
                    .catch(() => {
                        post = new Date();
                        post = post.getTime() / 1000;

                        const embedTimeOut = new Discord.RichEmbed()
                            .setColor("DARK_BLUE")
                            .setAuthor("Regular Roller")
                            .setDescription(`In order to save on resources a dice roll auto-cancelled after ${Math.round(responseTime / 1000)} seconds.`)
                            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
                            .setTimestamp();

                        msg.channel.send(embedTimeOut).then(() => {
                            sent.delete();
                        });
                    });
            });
        } else {
            var result = roller.roller(sidesOfDice, numOfDice, keepsOfDice, keepBefore, vicious, destructive, modifier);

            finalSum = result.pop();

            const arrSum = arr => arr.reduce((a, b) => a + b, 0);
            var temp = "";
            for (i = 0; i < sidesOfDice.length; i++) {
                temp += "d" + sidesOfDice[i].toString() + ":[";
                for (j = 0; j < numOfDice[i]; j++) {
                    temp += arrSum(result[i][j]);
                    if (result[i][j].length > 1) {
                        temp += " (";
                        for (k = 0; k < result[i][j].length; k++) {
                            temp += result[i][j][k] + " + ";
                        }
                        temp = temp.slice(0, temp.length - 3);
                        temp += ")";
                    }
                    temp += ", "
                }
                temp = temp.slice(0, temp.length - 2);
                temp += "] ";
            }

            var post = new Date();
            post = post.getTime() / 1000;

            const embed = new Discord.RichEmbed()
                .setColor("DARK_BLUE")
                .setAuthor("Regular Roller")
                .setTitle(`Outcome: ${finalSum}`)
                .setDescription(`Dice: ${temp}`)
                .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
                .setTimestamp()
                .addField("Other Parameters", `Vicious: ${vicious}\nDestructive: ${destructive}\nKeep Before: ${keepBefore}`, true);

            msg.channel.send(embed);
        }
    }
}