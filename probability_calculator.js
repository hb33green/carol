module.exports = {
    probabilityCalculator: (sidesOfDice, numOfDice, keepsOfDice, keepBefore, vicious, destructive, modifier, cr, count) => {
        var success = 0;
        var failure = 0;
        for (i = 0; i < count; i++) {
            var finalSum = 0;
            for (j = 0; j < sidesOfDice.length; j++) {
                var outcomes = [];
                var hasExploded = 0;
                for (k = 0; k < numOfDice[j]; k++) {
                    var firstRoll = Math.floor((Math.random() * sidesOfDice[j]) + 1)
                    if (keepBefore && hasExploded >= keepsOfDice[j]) {
                        outcomes = [...outcomes, ...[firstRoll]];
                        continue;
                    }
                    if (firstRoll % sidesOfDice[j] == 0 || (firstRoll % sidesOfDice[j] ==  sidesOfDice[j] - 1 && destructive)) {
                        hasExploded++;
                    }
                    while (firstRoll % sidesOfDice[j] == 0 || (firstRoll % sidesOfDice[j] ==  sidesOfDice[j] - 1 && destructive)) {
                        if (vicious && sidesOfDice[j] == 20) {
                            firstRoll = firstRoll + Math.max(Math.floor((Math.random() * sidesOfDice[j]) + 1), Math.floor((Math.random() * sidesOfDice[j]) + 1));
                        } else {
                            firstRoll = firstRoll + Math.floor((Math.random() * sidesOfDice[j]) + 1);
                        }
                    }
                    outcomes = [...outcomes, ...[firstRoll]];
                }
                if (keepsOfDice[j] < 0) {
                    outcomes.sort((a, b) => a - b);
                } else if (keepsOfDice[j] > 0) {
                    outcomes.sort((a, b) => b - a);
                }
                var individualSum = 0;
                for (k = 0; k < Math.abs(keepsOfDice[j]); k++) {
                    individualSum += outcomes[k];
                }
                finalSum = finalSum + individualSum;
            }
            finalSum = finalSum + modifier;
            if (finalSum >= cr) {
                success++;
            } else {
                failure++;
            }
        }
        failure /= count;
        success /= count;
        failure *= 100;
        success *= 100;

        return [Math.round(failure), Math.round(success)];
    }
}