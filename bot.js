const defSettings = require("./settings.json");
const Discord = require("discord.js");

const change = require("./change.js");
const help = require("./help.js");
const info = require("./info.js");
const luck = require("./luck.js");
const olRoll = require("./ol_roll.js");
const roll = require("./roll.js");
const settings = require("./settings.js");


const client = new Discord.Client();

client.on("ready", async () => {
    console.log(`Logged in as ${client.user.username}!`);
    client.user.setActivity('Type \'CAROL -h\'');

    try {
        let link = await client.generateInvite(["SEND_MESSAGES", "SEND_TTS_MESSAGES", "MANAGE_MESSAGES", "EMBED_LINKS", "ATTACH_FILES", "READ_MESSAGE_HISTORY", "MENTION_EVERYONE", "USE_EXTERNAL_EMOJIS", "ADD_REACTIONS"]);
        console.log(link);
    } catch (err) {
        console.log(err.stack);
    }
});

client.login(defSettings.token);

try {
    client.on("message", async msg => {
        var pre = new Date();
        pre = pre.getTime() / 1000;

        if (msg.author.bot) return;

        if (msg.channel.type === "dm") return;

        let msgArr = msg.content.split(" ");
        let prefix = msgArr[0].toLowerCase();
        let args = msgArr.slice(1);

        if (defSettings.prefix !== prefix) return;

        for (i = 0; i < args.length; i++) {
            if (args[i] == "") {
                args.splice(i, 1);
                i--;
            }
        }

        if (args.length === 0) {
            msg.reply("\`\`\`Incorrect number of arguments. Try typing \'CAROL -h\' for help.\`\`\`");
            return;
        }

        switch (args[0].toLowerCase()) {
            case "-c":
                change.change(msg, pre);
                break;
            case "-h":
                help.help(msg, pre);
                break;
            case "-i":
                info.info(msg, pre, args.slice(1));
                break;
            case "-l":
                luck.luck(msg, pre, args.slice(1));
                break;
            case "-o":
                olRoll.olRoll(msg, pre, args.slice(1));
                break;
            case "-r":
                roll.roll(msg, pre, args.slice(1));
                break;
            case "-s":
                settings.settings(msg, pre, args.slice(1));
                break;
            default:
                msg.reply("\`\`\`Unknown argument. Try typing \'CAROL -h\' for help.\`\`\`")
                break;
        }
    });
} catch (err) {
    console.log(err);
}