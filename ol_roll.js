const Discord = require("discord.js");
const prob = require("./probability_calculator.js");
const help = require("./help.js");
const botSettings = require("./bot_settings.json");
const roller = require("./roller.js");
const fs = require("fs");

module.exports = {
    olRoll: (msg, pre, args) => {
        if (args.length <= 0) {
            help.helpO(msg, pre)
            return;
        }

        let rawSession = fs.readFileSync("./session_luck.json");
        let sessionLucky = JSON.parse(rawSession);
        let raw = fs.readFileSync("./luck.json");
        let lucky = JSON.parse(raw);

        var roll = botSettings.yes;
        var cancel = botSettings.no;
        var responseTime = botSettings["response time"];

        var lvl;
        var adv = 0;
        var cr;
        var lp = 0;
        var vicious = false;
        var destructive = false;

        try {
            for (i = 0; i < args.length; i++) {
                args[i] = args[i].toLowerCase();
                if (args[i].includes("+")) {
                    var temp = args[i];
                    temp = temp.split("+");
                    if (temp[0] != "" || adv != 0) {
                        throw null;
                    }
                    temp = temp[temp.length - 1];
                    temp = parseInt(temp, 10);
                    if (isNaN(temp)) {
                        throw null;
                    } else {
                        adv = temp;
                    }
                } else if (args[i].includes("-")) {
                    var temp = args[i];
                    temp = temp.split("-");
                    if (temp[0] != "" || adv != 0) {
                        throw null;
                    }
                    temp = temp[temp.length - 1];
                    temp = parseInt(temp, 10);
                    if (isNaN(temp)) {
                        throw null;
                    } else {
                        adv = -1 * temp;
                    }
                } else if (args[i].includes("c")) {
                    var temp = args[i];
                    temp = temp.split("c");
                    if (temp[0] != "" || cr != null) {
                        throw null;
                    }
                    temp = temp[temp.length - 1];
                    temp = parseInt(temp, 10);
                    if (isNaN(temp)) {
                        throw null;
                    } else {
                        cr = temp;
                    }
                } else if (args[i].includes("l")) {
                    var temp = args[i];
                    temp = temp.split("l");
                    if (temp[0] != "" || lp != 0) {
                        throw null;
                    }
                    temp = temp[temp.length - 1];
                    temp = parseInt(temp, 10);
                    if (isNaN(temp)) {
                        throw null;
                    } else {
                        lp = temp;
                    }
                } else if (args[i].includes("v")) {
                    var temp = args[i];
                    if (temp.length > 1 || vicious) {
                        throw null;
                    }
                    vicious = true;
                } else if (args[i].includes("t")) {
                    if (destructive || args[i].length > 1) {
                        throw null;
                    }
                    destructive = true;
                } else {
                    var temp = args[i];
                    if (lvl != null) {
                        throw null;
                    }
                    temp = parseInt(temp, 10);
                    if (isNaN(temp) || temp > 10) {
                        throw null;
                    } else {
                        lvl = temp;
                    }
                }
            }
        } catch {
            help.helpO(msg, pre);
            return;
        }

        var sidesOfDice = [20];
        var numOfDice = [1, 1];

        if (lvl == null) {
            help.helpO(msg, pre);
            return;
        } else {
            switch (lvl) {
                case 1:
                    sidesOfDice = [...sidesOfDice, ...[4]];
                    break;
                case 2:
                    sidesOfDice = [...sidesOfDice, ...[6]];
                    break;
                case 3:
                    sidesOfDice = [...sidesOfDice, ...[8]];
                    break;
                case 4:
                    sidesOfDice = [...sidesOfDice, ...[10]];
                    break;
                case 5:
                    sidesOfDice = [...sidesOfDice, ...[6]];
                    numOfDice[1] += 1;
                    break;
                case 6:
                    sidesOfDice = [...sidesOfDice, ...[8]];
                    numOfDice[1] += 1;
                    break;
                case 7:
                    sidesOfDice = [...sidesOfDice, ...[10]];
                    numOfDice[1] += 1;
                    break;
                case 8:
                    sidesOfDice = [...sidesOfDice, ...[8]];
                    numOfDice[1] += 2;
                    break;
                case 9:
                    sidesOfDice = [...sidesOfDice, ...[10]];
                    numOfDice[1] += 2;
                    break;
                case 10:
                    sidesOfDice = [...sidesOfDice, ...[8]];
                    numOfDice[1] += 3;
                    break;
                default:
                    numOfDice = [1];
                    break;
            }
        }

        var keepsOfDice = [...numOfDice];
        if (adv < 0) {
            keepsOfDice[1] *= -1;
        }
        numOfDice[numOfDice.length - 1] += Math.abs(adv) + lp;
        var modifier = lp;
        let rawS = fs.readFileSync("./bot_settings.json");
        let sett = JSON.parse(rawS);
        // var keepBefore = botSettings["keep before"];
        // var count = botSettings.count;
        var keepBefore = sett["keep before"];
        var count = sett.count;

        if (cr != null) {
            results = prob.probabilityCalculator(sidesOfDice, numOfDice, keepsOfDice, keepBefore, vicious, destructive, modifier, cr, count);

            var post = new Date();
            post = post.getTime() / 1000;

            const embed = new Discord.RichEmbed()
                .setColor("DARK_GREEN")
                .setAuthor("Open Legend Roller")
                .setTitle(`${results[1]}% Chance of Success.`)
                .setDescription(`You have ${Math.round(responseTime / 1000)} seconds to respond before this roll auto-cancels.`)
                .addField("Success Rate", `${results[1]}%`, true)
                .addField("Failure Rate", `${results[0]}%`, true)
                .addField("Challenge Rating", `${cr}`, true)
                .addField("Level", `${lvl}`, true)
                .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
                .setTimestamp();

            if (adv > 0) {
                embed.addField("Advantage", `${adv}`, true);
            } else if (adv < 0) {
                embed.addField("Disadvantage", `${Math.abs(adv)}`, true)
            }

            if (lp != 0) {
                embed.addField("Legend Points", `${lp}`, true)
            }

            embed.addField("Other Parameters", `Vicious: ${vicious}\nDestructive: ${destructive}\nKeep Before: ${keepBefore}\nCount: ${count}`, true)

            msg.channel.send(embed).then(sent => {
                sent.react(roll).then(() => {
                    sent.react(cancel);
                });
                sent.awaitReactions((reaction, user) => { return (reaction.emoji.name === roll || reaction.emoji.name === cancel) && user.id === msg.author.id; },
                    { max: 1, time: responseTime, errors: ["time"] })
                    .then(collected => {
                        if (collected.firstKey() == roll) {
                            var hasData = false;
                            var hasSessionData = false;
                            for (i = 0; i < sessionLucky.users.length; i++) {
                                if (sessionLucky.users[i].hasOwnProperty(msg.author.id)) {
                                    hasSessionData = true;
                                }
                            }
                            for (i = 0; i < lucky.users.length; i++) {
                                if (lucky.users[i].hasOwnProperty(msg.author.id)) {
                                    hasData = true;
                                }
                            }

                            var result = roller.roller(sidesOfDice, numOfDice, keepsOfDice, keepBefore, vicious, destructive, modifier);

                            finalSum = result.pop();

                            const arrSum = arr => arr.reduce((a, b) => a + b, 0);
                            var temp = "";
                            for (i = 0; i < sidesOfDice.length; i++) {
                                temp += "d" + sidesOfDice[i].toString() + ":[";
                                for (j = 0; j < numOfDice[i]; j++) {
                                    temp += arrSum(result[i][j]);
                                    if (result[i][j].length > 1) {
                                        temp += " (";
                                        for (k = 0; k < result[i][j].length; k++) {
                                            temp += result[i][j][k] + " + ";
                                        }
                                        temp = temp.slice(0, temp.length - 3);
                                        temp += ")";
                                    }
                                    temp += ", "
                                }
                                temp = temp.slice(0, temp.length - 2);
                                temp += "] ";
                            }

                            if (!hasData) {
                                var newObj = {
                                    [msg.author.id]: {
                                        "success percent": 0,
                                        "success count": 0,
                                        "failure percent": 0,
                                        "failure count": 0
                                    }
                                }

                                lucky.users.push(newObj);
                            }

                            if (!hasSessionData) {
                                var newObj = {
                                    [msg.author.id]: {
                                        "success percent": 0,
                                        "success count": 0,
                                        "failure percent": 0,
                                        "failure count": 0
                                    }
                                }

                                sessionLucky.users.push(newObj);
                            }

                            var outcomeString = "";
                            if (finalSum >= cr) {
                                outcomeString = "Success!";
                                for (i = 0; i < sessionLucky.users.length; i++) {
                                    if (sessionLucky.users[i].hasOwnProperty(msg.author.id)) {
                                        sessionLucky.users[i][msg.author.id]["success percent"] += results[1];
                                        sessionLucky.users[i][msg.author.id]["success count"] += 1;
                                        break;
                                    }
                                }
                                for (i = 0; i < lucky.users.length; i++) {
                                    if (lucky.users[i].hasOwnProperty(msg.author.id)) {
                                        lucky.users[i][msg.author.id]["success percent"] += results[1];
                                        lucky.users[i][msg.author.id]["success count"] += 1;
                                        break;
                                    }
                                }
                            } else {
                                outcomeString = "Failure...";
                                for (i = 0; i < sessionLucky.users.length; i++) {
                                    if (sessionLucky.users[i].hasOwnProperty(msg.author.id)) {
                                        sessionLucky.users[i][msg.author.id]["failure percent"] += results[0];
                                        sessionLucky.users[i][msg.author.id]["failure count"] += 1;
                                        break;
                                    }
                                }
                                for (i = 0; i < lucky.users.length; i++) {
                                    if (lucky.users[i].hasOwnProperty(msg.author.id)) {
                                        lucky.users[i][msg.author.id]["failure percent"] += results[0];
                                        lucky.users[i][msg.author.id]["failure count"] += 1;
                                        break;
                                    }
                                }
                            }

                            let backToRawSession = JSON.stringify(sessionLucky, null, 2);
                            let backToRaw = JSON.stringify(lucky, null, 2);
                            fs.writeFileSync("./session_luck.json", backToRawSession);
                            fs.writeFileSync("./luck.json", backToRaw);

                            post = new Date();
                            post = post.getTime() / 1000;

                            const embedCont = new Discord.RichEmbed()
                                .setColor("DARK_GREEN")
                                .setAuthor("Open Legend Roller")
                                .setTitle(`${outcomeString} ${finalSum}`)
                                .setDescription(`Dice: ${temp}`)
                                .addField("Success Rate", `${results[1]}%`, true)
                                .addField("Failure Rate", `${results[0]}%`, true)
                                .addField("Challenge Rating", `${cr}`, true)
                                .addField("Level", `${lvl}`, true)
                                .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
                                .setTimestamp();

                            if (adv > 0) {
                                embedCont.addField("Advantage", `${adv}`, true);
                            } else if (adv < 0) {
                                embedCont.addField("Disadvantage", `${Math.abs(adv)}`, true);
                            }

                            if (lp != 0) {
                                embedCont.addField("Legend Points", `${lp}`, true);
                            }

                            embedCont.addField("Other Parameters", `Vicious: ${vicious}\nDestructive: ${destructive}\nKeep Before: ${keepBefore}\nCount: ${count}`, true);

                            msg.channel.send(embedCont).then(() => {
                                sent.delete();
                            });
                        } else if (collected.firstKey() == cancel) {
                            sent.delete();
                            msg.react(cancel);
                        }
                    })
                    .catch(() => {
                        post = new Date();
                        post = post.getTime() / 1000;

                        const embedTimeOut = new Discord.RichEmbed()
                            .setColor("DARK_GREEN")
                            .setAuthor("Open Legend Roller")
                            .setDescription(`In order to save on resources a dice roll auto-cancelled after ${Math.round(responseTime / 1000)} seconds.`)
                            .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
                            .setTimestamp();

                        msg.channel.send(embedTimeOut).then(() => {
                            sent.delete();
                        });
                    });
            });
        } else {
            var result = roller.roller(sidesOfDice, numOfDice, keepsOfDice, keepBefore, vicious, destructive, modifier);

            finalSum = result.pop();

            const arrSum = arr => arr.reduce((a, b) => a + b, 0);
            var temp = "";
            for (i = 0; i < sidesOfDice.length; i++) {
                temp += "d" + sidesOfDice[i].toString() + ":[";
                for (j = 0; j < numOfDice[i]; j++) {
                    temp += arrSum(result[i][j]);
                    if (result[i][j].length > 1) {
                        temp += " (";
                        for (k = 0; k < result[i][j].length; k++) {
                            temp += result[i][j][k] + " + ";
                        }
                        temp = temp.slice(0, temp.length - 3);
                        temp += ")";
                    }
                    temp += ", "
                }
                temp = temp.slice(0, temp.length - 2);
                temp += "] ";
            }

            var post = new Date();
            post = post.getTime() / 1000;

            const embed = new Discord.RichEmbed()
                .setColor("DARK_GREEN")
                .setAuthor("Open Legend Roller")
                .setTitle(`Outcome: ${finalSum}`)
                .setDescription(`Dice: ${temp}`)
                .addField("Level", `${lvl}`, true)
                .setFooter(`Requested by ${msg.author.username} | Response Time ≈ ${(post - pre).toFixed(2)} seconds.`, `${msg.author.displayAvatarURL}`)
                .setTimestamp();

            if (adv > 0) {
                embed.addField("Advantage", `${adv}`, true);
            } else if (adv < 0) {
                embed.addField("Disadvantage", `${Math.abs(adv)}`, true);
            }

            if (lp != 0) {
                embed.addField("Legend Points", `${lp}`, true);
            }

            embed.addField("Other Parameters", `Vicious: ${vicious}\nDestructive: ${destructive}\nKeep Before: ${keepBefore}`, true);

            msg.channel.send(embed);
        }
    }
}